# Slot Grid

![the slot grid in a floating window](screenshots/example_1.png)

A Godot container that reflows on resize, suitable for RPG inventories and the like.