extends Node2D

var texture := preload("res://icon.png")

func _ready():
	$WindowDialog.show();
	for i in 30:
		var icon := TextureRect.new()
		var label := Label.new()
		label.text = str(i)
		icon.texture = texture
		icon.expand = true
		icon.stretch_mode = TextureRect.STRETCH_KEEP_ASPECT
		icon.add_child(label)
		$WindowDialog/PanelContainer/ScrollContainer/SlotContainer.add_child(icon)
	
