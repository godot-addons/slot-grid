extends Container

# Exportable Properties
var cell_size := Vector2(64,64) setget set_cell_size
var margin_size := Vector2(5,5) setget set_margin_size
var padding_size := Vector2(8,8) setget set_padding_size
var warp_early := false setget set_warp_early
var item_style_box: StyleBox setget set_item_style_box
var draw_background := true setget set_draw_background

# read-only properties
var lattice: Lattice = null setget _read_only, get_lattice

# cache
var _size_last_time: Vector2


func _ready():
	var _connected = connect("resized", self, "_on_resized_maybe_sort_children")


func _notification(what: int):
	if what == NOTIFICATION_SORT_CHILDREN:
		_reset_lattice()
		_sort_children()


func _on_resized_maybe_sort_children():
	if (_size_last_time - rect_size).abs() >= cell_size:
		_reset_lattice()
		_size_last_time = rect_size
		_sort_children()
		update()

# Lattice is cached after the first generation; set it to null to generate it again
# on the next call
func get_lattice() -> Lattice:
	if lattice == null:
		lattice = Lattice.new(rect_size, cell_size + margin_size, warp_early)
	return lattice


# empties the lattice so it gets generated again on the next call to `get_lattice`
func _reset_lattice():
	lattice = null


func _draw():
	if not draw_background:
		return
	var style_box := _get_a_style_box()
	var _lattice := get_lattice()
	var block_size = cell_size + margin_size
	for lattice_pos in _lattice:
		var position = lattice_pos.to_vector2(block_size)
		var rect := Rect2(position, cell_size);
		draw_style_box(style_box, rect)


# If no stylebox is set, generate one for drawing the background of the lattice
func _get_a_style_box() -> StyleBox:
	if item_style_box != null:
		return item_style_box
	var default_style_box := StyleBoxFlat.new()
	var color := Color(0, 0, 0, 20)
	var corner := 3
	default_style_box.bg_color = color
	default_style_box.corner_radius_bottom_left = corner
	default_style_box.corner_radius_bottom_right = corner
	default_style_box.corner_radius_top_left = corner
	default_style_box.corner_radius_top_right = corner
	return default_style_box


# Verifies the passed child should be handled by the grid
func _child_is_valid(child: Node) -> bool:
	if not child.has_method("get_combined_minimum_size"):
		return false
	if not child.visible:
		return false
	return true


func _sort_children():
	
	var child_size = cell_size - padding_size
	var block_size = cell_size + margin_size
	
	var _lattice := get_lattice()
	_lattice.reset_iterator()
	
	for child in get_children():
		if not _child_is_valid(child):
			continue
		
		var child_position := _lattice.get_current().to_vector2(block_size)
		fit_child_in_rect(child, Rect2(child_position + padding_size/2, child_size))
		_lattice.next()


# Poor man's read only setter
func _read_only(_val) -> void:
	push_error("read only field")


func set_cell_size(new_size: Vector2):
	_reset_lattice()
	cell_size = new_size


func set_margin_size(new_size: Vector2):
	_reset_lattice()
	margin_size = new_size


func set_padding_size(new_size: Vector2):
	_reset_lattice()
	padding_size = new_size


func set_warp_early(do_warp: bool):
	_reset_lattice()
	warp_early = do_warp


func set_item_style_box(new_style_box: StyleBox) -> void:
	_reset_lattice()
	item_style_box = new_style_box
	update()


func set_draw_background(new_draw_background: bool) -> void:
	draw_background = new_draw_background
	update()

###############################################################################
#
#  UTILITY CLASSES
#
###############################################################################


# While waiting for Godot 4...
class Vector2i:
	var x := 0
	var y : = 0
	
	
	func _init(new_x: int, new_y: int):
		x = new_x
		y = new_y
	
	
	func to_vector2(scale_to: Vector2 = Vector2.ZERO) -> Vector2:
		var vec := Vector2(x, y)
		if not scale_to:
			return vec
		return (vec * scale_to)
	
	
	func _to_string():
		return '(i %s, %s)'%[x, y];


class Lattice:
	
	var rows := 0
	var cols := 0
	var cell_size := Vector2()
	var _current_row := 0
	var _current_col := 0
	
	
	func _init(area_size: Vector2, new_cell_size: Vector2, warp_early: bool):
		cell_size = new_cell_size
		var _rows := area_size.y / cell_size.y
		rows = int(floor(_rows) if warp_early else _rows)
		var _cols := area_size.x / cell_size.x
		cols = int(floor(_cols) if warp_early else _cols)
	
	
	func _to_string():
		return '(g %s, %s)'%[cols, rows];
	
	
	func is_equal(g: Lattice) -> bool:
		return (g.rows == rows and g.cols == cols)
	
	
	func reset_iterator():
		_current_row = 0
		_current_col = 0
	
	
	func next():
		_current_col += 1
		if _current_col >= cols:
			_current_col = 0
			_current_row += 1
	
	
	func get_current() -> Vector2i:
		return Vector2i.new(_current_col, _current_row)
	
	
	func should_continue() -> bool:
		return _current_col <= cols and _current_row <= rows 
	
	
	func _iter_init(_arg: Array) -> bool:
		#prints("_iter_init", _arg[0] is Lattice)
		reset_iterator()
		return should_continue()
	
	
	func _iter_next(_arg) -> bool:
		#prints("_iter_next", _arg[0] is Lattice)
		next()
		return should_continue()
	
	
	func _iter_get(_arg: Lattice) -> Vector2i:
		#prints("_iter_get", _arg)
		return get_current()
